from torch.utils.data import Dataset, DataLoader
from utils.helping_fn import *
import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from pathlib import Path


def get_pretrain_transcript(file):
    with open(file, 'r') as transcript:
        # Skip first 4 lines
        for _ in range(4):
            transcript.readline()
        res = [line.split()[:-1] for line in transcript]
    return res


class PretrainDataLoader:
    def __init__(self, config):
        batch_size = 40
        dataset_train = AudioDataset(config.pretrain_manifest_path, batch_size, config)
        self.train_data = DataLoader(dataset_train, batch_size=batch_size, collate_fn=dataset_train.collate,
                                     pin_memory=True, num_workers=4, )


class AudioDataset(Dataset):
    def __init__(self, root_path, batch_size, config):
        self.config = config
        self.files = files_from_manifest(root_path)
        self.batch_size = batch_size
        self.file_idx = 0
        self.world_index = 0
        self.max_idx = 0

    def __len__(self):
        return 2064048 - 2064048 % self.batch_size  # Number of words in dataset aligned to batch size

    def __getitem__(self, idx):
        labels = get_pretrain_transcript(self.config.dataset_pretrain_root + "/" + self.files[self.file_idx] + ".txt")
        label, start, end = labels[self.world_index]
        label = labels_in_numpy(label)[0]

        mfcc = np.load(Path(self.config.dataset_pretrain_root + "/" + self.files[self.file_idx] + "_mfcc.npy"))
        mfcc = mfcc[int(float(start) * 100):int(float(end) * 100) + 1]

        if self.world_index + 1 == len(labels):
            self.file_idx += 1
            self.world_index = 0
        else:
            self.world_index += 1

        return torch.tensor(mfcc).float(), len(mfcc), torch.tensor(label), len(label)

    def collate(self, batch):
        mfcc = pad_sequence([item[0] for item in batch])
        mfcc = mfcc.permute(1, 0, 2)
        mfcc_lens = torch.tensor([item[1] for item in batch])
        labels = pad_sequence([item[2] for item in batch])
        labels = labels.permute(1, 0)
        label_lens = torch.tensor([item[3] for item in batch])

        return mfcc.float(), mfcc_lens, labels, label_lens



