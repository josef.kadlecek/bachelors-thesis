from model import Encoder_CNN_LSTM
from video_model import VideoModel
import torch
from tests_utils import test_decorator

NUMVER_OF_CLASSES = 44
INPUT_IMAGE_SIZE = [32, 32]
MFCC_SIZE = 13
BATCH_SIZE = 16
SAMPLE_LEN = 42


@test_decorator
def test_audio_model_flow():
    # Init model
    model = Encoder_CNN_LSTM(NUMVER_OF_CLASSES)
    # Random input
    input = torch.randn(BATCH_SIZE, SAMPLE_LEN, MFCC_SIZE)
    out = model(input)

    # Check output sizes
    assert out.size()[0] == SAMPLE_LEN // model.pooling_size, \
        f"Output size missmatch. Expected sample len {SAMPLE_LEN // 2} got {out.size()[0]}"
    assert out.size()[1] == BATCH_SIZE, \
        f"Output size missmatch. Expected batch size {BATCH_SIZE} got {out.size()[1]}"
    assert out.size()[2] == NUMVER_OF_CLASSES, \
        f"Output size missmatch. Expected num of classes {NUMVER_OF_CLASSES} got {out.size()[2]}"


@test_decorator
def test_video_model_flow():
    # Init model
    model = VideoModel(NUMVER_OF_CLASSES)

    # Random input
    input = torch.randn(SAMPLE_LEN, BATCH_SIZE, 1, INPUT_IMAGE_SIZE[0], INPUT_IMAGE_SIZE[1])
    out = model(input)

    # Check output sizes
    assert out.size()[0] == SAMPLE_LEN, \
        f"Output size missmatch. Expected sample len {SAMPLE_LEN // 2} got {out.size()[0]}"
    assert out.size()[1] == BATCH_SIZE, \
        f"Output size missmatch. Expected batch size {BATCH_SIZE} got {out.size()[1]}"
    assert out.size()[2] == NUMVER_OF_CLASSES, \
        f"Output size missmatch. Expected num of classes {NUMVER_OF_CLASSES} got {out.size()[2]}"


def run_all():
    test_video_model_flow()
    test_audio_model_flow()


if __name__ == "__main__":
    run_all()
