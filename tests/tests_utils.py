def test_decorator(called_test):
    def wrapper():
        test_name = called_test.__name__
        try:
            called_test()
            print(f"Test {test_name} OK")
        except AssertionError as failure:
            print(f"Test {test_name} failed. Reason:")
            print(failure)
        except Exception as total_failure:
            print(f"ALERT. Test {test_name} returned unexpected exception.")
            print(total_failure)

    return wrapper
