from config import Config
from dataloader import MyDataLoader
from tests_utils import test_decorator
from video_data_loader import VideoDataLoader
import torch

MFCC_SIZE = 13
BATCH_SIZE = 16
CONFIG = Config(batch_size=BATCH_SIZE)


@test_decorator
def test_audio_data_loader(test_all_files=False):
    data_loader = MyDataLoader(CONFIG)
    for data, data_len, label, label_len in data_loader.train_data:
        assert data.size()[0] == BATCH_SIZE, "Dataloader corrupted MFCC"
        assert data.size()[2] == MFCC_SIZE, "Dataloader corrupted MFCC"
        if test_all_files == False:
            break


@test_decorator
def test_video_data_loader(test_all_files=False):
    data_loader = VideoDataLoader(CONFIG)
    for data, data_len, label, label_len in data_loader.train_data:
        assert data.size()[1:] == torch.Size([BATCH_SIZE, 1, 32, 32]), "Dataloader corrupted frame loading."
        if test_all_files == False:
            break


def run_all():
    test_audio_data_loader()
    test_video_data_loader()


if __name__ == "__main__":
    run_all()
