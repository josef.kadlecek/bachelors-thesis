import os
import shutil
import sys
import matplotlib
from matplotlib import pyplot as plt
import torch
import math
import numpy as np
from numpy import inf



if __name__ == "__main__":
    # data1 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\before\\train_cer_history.npy")
    # data2 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\before\\validation_cer_history.npy")
    #
    # data3 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\increased_output\\train_cer_history.npy")
    # data4 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\increased_output\\validation_cer_history.npy")
    #
    # data5 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\increased_input\\train_cer_history.npy")
    # data6 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\increased_input\\validation_cer_history.npy")
    #
    # data7 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\no_input_increased_output\\train_cer_history.npy")
    # data8 = np.load("C:\\Users\\Josef\\Desktop\\convsizes\\no_input_increased_output\\validation_cer_history.npy")


    data1 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\02\\train_cer_history.npy")
    data2 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\02\\validation_cer_history.npy")

    data3 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\03\\train_cer_history.npy")
    data4 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\03\\validation_cer_history.npy")

    data5 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\05\\train_cer_history.npy")
    data6 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\05\\validation_cer_history.npy")

    data7 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\06\\train_cer_history.npy")
    data8 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\06\\validation_cer_history.npy")

    data9 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\04\\train_cer_history.npy")
    data0 = np.load("C:\\Users\\Josef\\Desktop\\audio_drops\\04\\validation_cer_history.npy")

    delka = max(  len(data1), len(data3), len(data5) )

    # data1 = data1[:delka]
    # data2 = data2[:delka]
    # data3 = data3[:delka]
    # data4 = data4[:delka]
    # data5 = data5[:delka]
    # data6 = data6[:delka]
    # data7 = data7[:delka]
    # data8 = data8[:delka]
    # data9 = data9[:delka]
    # data0 = data0[:delka]

    updates_per_batch = 1039
    # Plot Loss

    fig = plt.figure(figsize=(9.25, 5.25))
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    # plt.title(title)
    plt.ylabel("CER")
    plt.xlabel("Updates")



    axes.plot([(i + 1) * updates_per_batch  for i in range(len(data1))], data1, 'C0:', label="0.2 DROPOUT Training")
    axes.plot([(i + 1) * updates_per_batch  for i in range(len(data2))], data2, 'C0-', label="0.2 DROPOUT Validation")

    axes.plot([(i + 1) * updates_per_batch  for i in range(len(data3))], data3, 'C1:',label="0.3 DROPOUT Training")
    axes.plot([(i + 1) * updates_per_batch  for i in range(len(data4))], data4, 'C1-',label="0.3 DROPOUT Validation")

    # updates_per_batch = 1039
    axes.plot([(i + 1) * updates_per_batch for i in range(len(data9))], data9, 'C4:',label="0.4 DROPOUT Training")
    axes.plot([(i + 1) * updates_per_batch for i in range(len(data0))], data0, 'C4-',label="0.4 DROPOUT Validation")

    axes.plot([(i + 1) * updates_per_batch for i in range(len(data5))], data5, 'C2:',label="0.5 DROPOUT Training")
    axes.plot([(i + 1) * updates_per_batch for i in range(len(data6))], data6, 'C2-',label="0.5 DROPOUTValidation")

    axes.plot([(i + 1) * updates_per_batch for i in range(len(data7))], data7, 'C3:',label="0.6 DROPOUT Training")
    axes.plot([(i + 1) * updates_per_batch for i in range(len(data8))], data8, 'C3-',label="0.6 DROPOUT Validation")

    plt.legend()

    plt.show()
    plt.close('all')
