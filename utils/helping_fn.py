import os
import csv
import numpy as np
from .config import DEVICE, ALPHABET


def files_from_manifest(manifest_path):
    all_files = []
    with open(manifest_path, 'r') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        for row in csv_reader:
            all_files.append(row[0])
    return all_files


def get_transcript(file):
    with open(file, 'r') as transcript:
        for line in transcript:
            return line[7:-1]


def labels_in_numpy(text):
    values_in_int = []
    values_in_int.append([ALPHABET[c] for c in text])
    return np.array(values_in_int)


def show_images_from_numpy(np_array):
    images = np.load(np_array)
    for im in images:
        show_img(im)


def show_img(img):
    import cv2
    img = img.astype('uint8')
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def data_to_device(mfcc, mfcc_len, label, label_len):
    mfcc = mfcc.to(DEVICE)
    mfcc_len = mfcc_len.to(DEVICE)
    label = label.to(DEVICE)
    label_len = label_len.to(DEVICE)
    return mfcc, mfcc_len, label, label_len


def check_manifest_for_large_files(manifest, outfile):
    with open(outfile, 'w+') as outfile:
        files = files_from_manifest(manifest)

        for i in range(len(files)):
            if os.path.isfile(files[i].replace("whole_audio", "main") + "_mfcc.npy"):
                files[i] = (files[i], os.path.getsize(files[i].replace("whole_audio", "main") + "_mfcc.npy"))
                if files[i][1] < 300000:
                    name = files[i][0].replace("whole_audio", "main")
                    outfile.write(f"{name}\n")
                else:
                    print(f"FILE TOO LARGE {files[i][0]}\n")

            elif os.path.isfile(files[i] + "_mfcc.npy"):
                files[i] = (files[i], os.path.getsize(files[i] + "_mfcc.npy"))
                if files[i][1] < 300000:
                    outfile.write(f"{files[i][0]}\n")
                else:
                    print(f"FILE TOO LARGE {files[i][0]}\n")

            else:
                outfile.write(f"File not found" + files[i] + "_mfcc.npy" + "\n")
