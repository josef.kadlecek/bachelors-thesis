from __future__ import print_function, division
import os
import numpy as np
import csv
from python_speech_features import mfcc
import soundfile as sf
import subprocess
from config import Config
from helping_fn import num_to_text


def get_all_files(root_dir, type):
    all_files = []
    for path, _, files in os.walk(root_dir):
        for name in files:
            if (name.endswith(type)):
                all_files.append(os.path.join(path, name.replace(type, "")))
    return all_files


def convert_to_mffc(file_path):
    data, sample_rate = sf.read(file_path + ".wav")
    mfcc_features = mfcc(data, sample_rate)
    np.save(file_path + "_mfcc", mfcc_features)


def preprocess_dataset_to_mfcc(files):
    for num, file in enumerate(files):
        convert_to_mffc(file)
        # Todo: preprocess video
        if num % 1000 == 0:
            print("Processed:", num, "Of total:", len(files))


def create_manifest(files, manifest_name):
    with open(manifest_name, 'w', ) as csvfile:
        writer = csv.writer(csvfile, lineterminator="\n")
        for file in files:
            writer.writerow([file.replace(".mp4", "")])


def create_wavs_from_mp4(files):
    for num, file in enumerate(files):
        command = f"ffmpeg -loglevel panic -i {file + '.mp4'} {file + '.wav'}"
        subprocess.call(command, shell=True)

        if num % 1000 == 0:
            print("Processed:", num, "Of total:", len(files))


def sort_files(files):
    for i in range(len(files)):
        files[i] = (files[i], os.path.getsize(files[i] + ".mp4"))

    files.sort(key=lambda filename: filename[1])

    # Re-populate list with just filenames
    for i in range(len(files)):
        files[i] = files[i][0]

    return files


def change_numbers_to_text(files):
    for file in files:
        num_to_text(file + ".txt")


if __name__ == "__main__":
    CONFIG = Config()
    files = get_all_files(CONFIG.dataset_root, ".mp4")

    # change_numbers_to_text(files)

    # Main
    # train_files = sort_files(files[:38400])  # 80% dividable by 32
    # val_files = sort_files(files[38400:48160])  # 20% dividable by 32

    # Pretrain
    # train_files = sort_files(files[:77056])  # 80% dividable by 32
    # val_files = sort_files(files[77056:77056+19232])  # 20% dividable by 32
    #
    # create_manifest(train_files, CONFIG.training_manifest_path)
    # create_manifest(val_files, CONFIG.val_manifest_path)

    create_wavs_from_mp4(files)
    preprocess_dataset_to_mfcc(files)
