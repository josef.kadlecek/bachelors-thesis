# Torch transforms annd other stuff
# https://stackoverflow.com/questions/45113245/how-to-get-mini-batches-in-pytorch-in-a-clean-and-efficient-way
import platform
import torch
import os
from datetime import datetime
import platform

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Locations of dataset on diferent platforms
if platform.system() == 'Windows':
    PATH = 'D:\\Dataset\\'
elif platform.system() == 'Linux':
    PATH = '/data2/Dataset/mvlrs_v1/'
if platform.platform() == 'Linux-4.14.79+-x86_64-with-Ubuntu-18.04-bionic':
    PATH = "/content/drive/My Drive/Colab Notebooks/"

ALPHABET = {'#': 0, 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12,
            'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24,
            'Y': 25, 'Z': 26, '\'': 27, '.': 28, '!': 29, '?': 30, ':': 31, ',': 32, ' ': 33, '0': 34, '1': 35, '2': 36,
            '3': 37, '4': 38, '5': 39, '6': 40, '7': 41, '8': 42, '9': 43}


class Config():

    def __init__(self, path=PATH, batch_size=30, overfit=False):
        self.overfitting = overfit
        self.platform = path
        self.epochs = 500
        self.batch_size = batch_size
        self.alphabet = ALPHABET
        self.learning_rate = 0.0001
        self.device = DEVICE
        self.log_epoch = 1

        utils_path = os.path.dirname(os.path.abspath(__file__))
        self.experiment_folder = os.path.join(path, str(format(datetime.now(), '%d.%m.%H.%M')))
        self.training_manifest_path = os.path.join(utils_path, "training_manifest.csv")
        self.val_manifest_path = os.path.join(utils_path, "validation_manifest.csv")
        self.test_manifest_path = os.path.join(utils_path, "testing_manifest.csv")
        self.pretrain_manifest_path = os.path.join(utils_path, "pretrain_manifest.csv")
        if self.overfitting:
            print("OVERFITTING TURNED ON")
            self.training_manifest_path = os.path.join(utils_path, "..", "other", "overfit_manifest.csv")
            self.test_manifest_path = self.training_manifest_path
            self.val_manifest_path = self.training_manifest_path
        self.dataset_root = path + "main"
        self.dataset_pretrain_root = path + "pretrain_audio"
        self.video_dataset_root = path + "main_video"

    def __str__(self):
        return f"Epochs {self.epochs}, Batch size {self.batch_size}, LR {self.learning_rate}, Results: {self.experiment_folder}"
