import subprocess, os
import cv2
import numpy as np
from pathlib import Path

from config import Config
from helping_fn import files_from_manifest

CONFIG = Config()
DETECTOR = 'C:/Users/Josef/Desktop/Detektor'
PATH = "D:/Dataset/main/"


def show_images_from_numpy(file):
    images = np.load(file)
    for i in images:
        cv2.imshow('image', i)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def get_mouth_points(video):
    os.chdir(DETECTOR)
    output = str(subprocess.check_output(r"dllDemo.exe 4 8 " + video, shell=True))

    start_idx = output.find('DETECTION 0')
    frames = output[start_idx:].split("fps\\r\\")

    res = [image for image in frames]
    res = res[:-1]
    res = [i[:i.find(r"\r")] for i in res]
    return res


def get_boundaries(frame):
    points = frame.split(" ")[2:]
    points = [point.split(":") for point in points]

    idxes = [i for i in range(48, 68)]  # Idexes for mouth points
    x_max = max([int(points[i][0]) for i in idxes])
    x_min = min([int(points[i][0]) for i in idxes])
    y_max = max([int(points[i][1]) for i in idxes])
    y_min = min([int(points[i][1]) for i in idxes])

    width = int((x_max - x_min) / 2)
    height = int((y_max - y_min) / 2)
    y_center = y_max - y_min
    x_center = x_max - x_min
    return y_min - width + height, y_max + width - height, x_min, x_max


def process_video(video, save_file):
    frames = get_mouth_points(str(video))
    vidcap = cv2.VideoCapture(str(video))
    all_images = []

    for frame in frames:
        success, img = vidcap.read()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        y_min, y_max, x_min, x_max = get_boundaries(frame)

        # Crop image in square
        crop_img = img[y_min: y_max, x_min: x_max]
        # Resize it so all images in dataset are same
        crop_img = cv2.resize(crop_img, (80, 80))
        all_images.append(crop_img)

    np.save(save_file, all_images)

    # Check for misdetections
    success, img = vidcap.read()
    while success:
        with open("err2.txt", 'a+') as file:
            file.write(f"MissDetection Error: {video}\n")
        success, img = vidcap.read()


def prune_manifest_nonexisting_files(manifest_path, prefix="D:/main_video/", suffix="_lips.npy"):
    videos = files_from_manifest(manifest_path)
    with open(manifest_path, 'w') as f:
        for i, video in enumerate(videos):
            if os.path.isfile(prefix + video + suffix) == True:
                f.write(video + "\n")


def prune_manifest_blacklist(manifest_path, blacklist_path):
    videos = files_from_manifest(manifest_path)
    blacklist = files_from_manifest(blacklist_path)
    with open(manifest_path, 'w') as f:
        for i, video in enumerate(videos):
            if video not in blacklist:
                f.write(video + "\n")



def main():
    videos = files_from_manifest("./misdetected.csv")
    for i, video in enumerate(videos):
        try:
            video = PATH + video + ".mp4"
            out_file = video.replace(".mp4", "_lips")
            process_video(Path(video), Path(out_file))
            print("Ok")
        except Exception as e:
            print(e)
            with open("err.txt", 'a+') as file:
                file.write(f"{i}  Detection Error: {video}\n")
            print("Detection error", video)


if __name__ == '__main__':
    # main()
    prune_manifest_blacklist("./training_manifest.csv", "../other/short.csv")
    # show_images_from_numpy(Path(out_file))
