import Levenshtein
import torch
import torch.nn as nn
from torch import optim
from dataloader import MyDataLoader
from dataloader_pretrain import PretrainDataLoader
from logger import Logger
from model import Encoder_CNN_LSTM
from testing_and_validation import decode, validate_model, avg_loss, test_model
from utils.config import Config
import numpy as np
import math
from utils.helping_fn import data_to_device
import sys

def pretrain_model(model, config):
    dl = PretrainDataLoader(config)
    torch.manual_seed(0)
    lr = config.learning_rate
    loss_fn = nn.CTCLoss().to(config.device)
    optimizer = optim.Adam(model.parameters(), lr=lr)

    for epoch in range(5):
        print("Pretrain epoch: ", epoch + 1)
        for i, (mfcc, mfcc_len, label, label_len) in enumerate(dl.train_data):
            mfcc, mfcc_len, label, label_len = data_to_device(mfcc, mfcc_len, label, label_len)
            optimizer.zero_grad()
            model.train()

            model_output = model(mfcc)
            if not (model_output != model_output).any():
                loss = loss_fn(model_output, label, mfcc_len / model.pooling_size, label_len)
                loss_value = loss.data.cpu().numpy()
                if np.isnan(loss_value) or math.isinf(loss_value):
                    pass
                else:
                    loss.backward()
                    optimizer.step()




def train_model(model, dl, logger, config):
    torch.manual_seed(0)
    lr = config.learning_rate
    loss_fn = nn.CTCLoss().to(config.device)
    optimizer = optim.Adam(model.parameters(), lr=lr)
    print("Starting to train...")
    for epoch in range(config.epochs):
        training_losses = []
        cer = np.array([])

        for batch_num, (mfcc, mfcc_len, label, label_len) in enumerate(dl.train_data):
            mfcc, mfcc_len, label, label_len = data_to_device(mfcc, mfcc_len, label, label_len)
            optimizer.zero_grad()
            model.train()

            model_output = model(mfcc)
            loss = loss_fn(model_output, label, mfcc_len / model.pooling_size, label_len)

            # Check current CER without dropout
            model.eval()
            model_output = model(mfcc)
            curr_loss = loss.data.cpu().numpy()

            if np.isnan(curr_loss) or math.isinf(curr_loss):
                print("Alert nans or inf!!!")
            else:
                training_losses.append(curr_loss)
                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), 0.25)
                optimizer.step()

            # CER
            for i in range(model_output.size()[1]):
                gt, out = decode(model_output[:, i, :], label[i])
                cer = np.append(cer, Levenshtein.distance(gt, out) / len(gt) * 100)

        training_cer = np.mean(cer)
        training_loss = avg_loss(training_losses)
        validation_loss, validation_cer = validate_model(dl, model, loss_fn, config.batch_size)
        logger.log_losses_and_cer(training_loss, training_cer, validation_loss, validation_cer)
        logger.save_model(model, validation_cer)
        test_model(model, dl, logger, config.batch_size)


def init_model(config, model_path=None):
    model = Encoder_CNN_LSTM(len(config.alphabet))
    model.to(config.device)

    if model_path:
        model.load_state_dict(torch.load(model_path))

    return model


def main():
    config = Config(batch_size=42, path=sys.argv[1])

    dl = MyDataLoader(config)
    model = init_model(config)
    logger = Logger(config, model, dl)

    pretrain_model(model, config)
    train_model(model, dl, logger, config)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("Usage: python train.py 'path/to/dataset'")
    main()
