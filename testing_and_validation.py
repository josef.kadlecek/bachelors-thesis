import Levenshtein
import numpy as np
import torch

if __name__ == '__main__':
    from .utils.config import ALPHABET
    from .utils.helping_fn import data_to_device
else:
    from utils.config import ALPHABET
    from utils.helping_fn import data_to_device


def test_model(model, dl, logger, batch_size):
    model.eval()
    torch.no_grad()
    result = []
    cer = np.array([])
    for mfcc, mfcc_len, label, label_len in dl.test_data:
        mfcc, mfcc_len, label, label_len = data_to_device(mfcc, mfcc_len, label, label_len)

        model_output = model(mfcc)

        for i in range(model_output.size()[1]):
            gt, out = decode(model_output[:, i, :], label[i])
            result.append((gt, out))
            cer = np.append(cer, Levenshtein.distance(gt, out) / len(gt) * 100)

    logger.log_decoding(result, np.mean(cer))


def test_video_model(model, dl, logger, batch_size):
    model.eval()
    torch.no_grad()
    result = []
    cer = np.array([])
    for frames, frame_len, label, label_len in dl.test_data:
        frames, frame_len, label, label_len = data_to_device(frames, frame_len, label, label_len)

        model_output = model(frames)

        for i in range(model_output.size()[1]):
            gt, out = decode(model_output[:, i, :], label[i])
            result.append((gt, out))
            cer = np.append(cer, Levenshtein.distance(gt, out) / len(gt) * 100)

    logger.log_decoding(result, np.mean(cer))


def avg_loss(losses):
    return np.nanmean(np.array([x for x in losses]))


def validate_model(dl, model, loss_fn, batch_size):
    torch.no_grad()
    model.eval()
    losses = []
    cer = np.array([])

    for mfcc, mfcc_len, label, label_len in dl.val_data:
        mfcc, mfcc_len, label, label_len = data_to_device(mfcc, mfcc_len, label, label_len)

        model_output = model(mfcc)

        loss = loss_fn(model_output, label, mfcc_len / model.pooling_size, label_len)
        losses.append(loss.data.cpu().numpy())

        # CER
        for i in range(batch_size):
            gt, out = decode(model_output[:, i, :], label[i])
            cer = np.append(cer, Levenshtein.distance(gt, out) / len(gt) * 100)

    return avg_loss(losses), np.mean(cer)


def decode(model_output, ground):
    model_output = model_output.to("cpu")
    alphabet_to_dix = dict((v, k) for k, v in ALPHABET.items())
    decoded = []

    for frame in model_output:
        frame = frame.detach().numpy()
        idx = frame.argmax()
        decoded.append(alphabet_to_dix[idx])

    decoded = " ".join("".join(decoded).split())

    res = [' ']
    for i, c in enumerate(decoded):
        if res[-1] != c:
            res.append(c)

    out = ("".join(res[1:]))
    out = out.replace("#", "")

    gt = "Oida"
    if ground.type() != None:
        ground = ground.to("cpu")
        ground = ground.numpy().squeeze()
        label_decoded = [alphabet_to_dix[idx] for idx in ground]
        gt = ("".join(label_decoded))
        gt = gt.replace("#", "")
    return gt, out
