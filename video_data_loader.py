from torch.utils.data import Dataset, DataLoader
from utils.helping_fn import *
import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from pathlib import Path
import torchvision.transforms as transforms
from PIL import Image


class VideoDataLoader:
    def __init__(self, config):
        batch_size = config.batch_size
        dataset_train = VideoDataset(config.training_manifest_path, batch_size, config)
        dataset_val = VideoDataset(config.val_manifest_path, batch_size, config)
        dataset_test = VideoDataset(config.test_manifest_path, 1, config)

        if torch.cuda.is_available():
            self.train_data = DataLoader(dataset_train, batch_size=batch_size, collate_fn=dataset_train.collate,
                                         pin_memory=False, num_workers=4, shuffle=not config.overfitting)
            self.val_data = DataLoader(dataset_val, batch_size=batch_size, collate_fn=dataset_val.collate,
                                       pin_memory=True, num_workers=4, shuffle=not config.overfitting)
            self.test_data = DataLoader(dataset_test, batch_size=1, collate_fn=dataset_test.collate,
                                        pin_memory=True, num_workers=4, shuffle=not config.overfitting)
        else:
            self.train_data = DataLoader(dataset_train, batch_size=batch_size, collate_fn=dataset_train.collate)
            self.val_data = DataLoader(dataset_val, batch_size=batch_size, collate_fn=dataset_train.collate)
            self.test_data = DataLoader(dataset_test, batch_size=1, collate_fn=dataset_train.collate)


class VideoDataset(Dataset):
    def __init__(self, root_path, batch_size, config):
        self.config = config
        self.files = files_from_manifest(root_path)
        self.batch_size = batch_size
        self.overfitting = config.overfitting
        self.transformer = transforms.Compose([
            transforms.Resize((32, 32)),
            transforms.ToTensor(),
        ])

    def __len__(self):
        return len(self.files) - len(self.files) % self.config.batch_size


    def __getitem__(self, idx):
        lips = np.load(Path(self.config.video_dataset_root + "/" + self.files[idx] + "_lips.npy"))
        lips = np.insert(lips, 0, lips[0], 0)
        lips = [self.transformer(Image.fromarray(lips[i, :, :].astype('uint8'))) for i in range(len(lips))]
        lips = torch.stack(lips)
        label = get_transcript(self.config.video_dataset_root + "/" + self.files[idx] + ".txt")
        label = labels_in_numpy(label)[0]

        return lips, len(lips), torch.tensor(label), len(label)

    def collate(self, batch):
        frames = pad_sequence([item[0] for item in batch])  # [item[0] for item in batch]#
        frame_lens = torch.tensor([item[1] for item in batch])
        labels = pad_sequence([item[2] for item in batch])
        labels = labels.permute(1, 0)
        label_lens = torch.tensor([item[3] for item in batch])

        return frames, frame_lens, labels, label_lens
