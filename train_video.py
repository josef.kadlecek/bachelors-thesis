import Levenshtein
import torch
import torch.nn as nn
from torch import optim
from video_data_loader import VideoDataLoader
from logger import Logger
from video_model import VideoModel
from testing_and_validation import decode, validate_model, avg_loss, test_video_model
from utils.config import Config
import numpy as np
import math
from utils.helping_fn import data_to_device
import sys


def train_model(model, dl, logger, config):
    torch.manual_seed(0)
    lr = config.learning_rate
    loss_fn = nn.CTCLoss().to(config.device)
    optimizer = optim.Adam(model.parameters(), lr=lr)
    print("Starting to train...")
    for epoch in range(config.epochs):
        training_losses = []
        cer = np.array([])

        for batch_num, (frame, frames_count, label, label_len) in enumerate(dl.train_data):
            frame, frames_count, label, label_len = data_to_device(frame, frames_count, label, label_len)

            model.train()
            optimizer.zero_grad()

            model_output = model(frame)
            loss = loss_fn(model_output, label, frames_count, label_len)

            curr_loss = loss.data.cpu().numpy()

            if np.isnan(curr_loss) or math.isinf(curr_loss):
                print(f"Frame sq len: {frames_count.cpu().numpy()} label len {label_len.cpu().numpy()}")
                logger.alert("Alert nans or inf!!!")
            else:
                training_losses.append(curr_loss)
                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), 0.25)
                optimizer.step()

            model.eval()
            model_output = model(frame)
            # CER
            for i in range(model_output.size()[1]):
                gt, out = decode(model_output[:, i, :], label[i])
                cer = np.append(cer, Levenshtein.distance(gt, out) / len(gt) * 100)

        training_cer = np.mean(cer)
        training_loss = avg_loss(training_losses)
        validation_loss, validation_cer = validate_model(dl, model, loss_fn, config.batch_size)

        logger.log_losses_and_cer(training_loss, training_cer, validation_loss, validation_cer)
        logger.save_model(model, validation_cer)
        test_video_model(model, dl, logger, config.batch_size)


def init_model(config, model_path=None):
    model = VideoModel(len(config.alphabet))
    model.to(config.device)

    if model_path:
        model.load_state_dict(torch.load(model_path))

    return model

def main():
    config = Config(batch_size=30, path=sys.argv[1])
    dl = VideoDataLoader(config)
    model = init_model(config)
    logger = Logger(config, model, dl)

    train_model(model, dl, logger, config)


if __name__ == "__main__":
    main()
