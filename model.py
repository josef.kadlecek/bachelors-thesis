import torch.nn as nn


class Encoder_CNN_LSTM(nn.Module):
    def __init__(self, embedding_dim, rnn_type=nn.LSTM, rnn_size=1024, dropout=0.4):
        super(Encoder_CNN_LSTM, self).__init__()
        self.pooling_size = 2

        self.conv_layers = nn.Sequential(

            nn.Conv1d(in_channels=13, out_channels=32, kernel_size=3, padding=1),  
            nn.BatchNorm1d(32),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Dropout(0.1),

            nn.Conv1d(in_channels=32, out_channels=64, kernel_size=3, padding=1),  
            nn.BatchNorm1d(64),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Dropout(0.1),

            nn.Conv1d(in_channels=64, out_channels=128, kernel_size=3, padding=1),  
            nn.BatchNorm1d(128),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Dropout(0.1),

            nn.Conv1d(in_channels=128, out_channels=256, kernel_size=3, padding=1),
            nn.BatchNorm1d(256),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Dropout(0.1),
            # Pool
            nn.MaxPool1d(self.pooling_size),
        )

        self.rnn1 = rnn_type(input_size=256, hidden_size=rnn_size // 2, batch_first=True,
                             bidirectional=True)
        self.rnn2 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, batch_first=True,
                             bidirectional=True)
        self.rnn3 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, batch_first=True,
                             bidirectional=True)
        self.rnn4 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, batch_first=True,
                             bidirectional=True)
        self.rnn5 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, batch_first=True,
                             bidirectional=True)

        self.drop1 = nn.Dropout(dropout)
        self.drop2 = nn.Dropout(dropout)
        self.drop3 = nn.Dropout(dropout)
        self.drop4 = nn.Dropout(dropout)
        self.drop5 = nn.Dropout(dropout)


        self.post_rn_conv = nn.Sequential(
            nn.BatchNorm1d(rnn_size),
            nn.LeakyReLU(),
            nn.Dropout(0.1),
            nn.Conv1d(in_channels=rnn_size, out_channels=rnn_size, kernel_size=3, padding=1),
            nn.BatchNorm1d(rnn_size),
            nn.LeakyReLU(),
            nn.Conv1d(in_channels=rnn_size, out_channels=embedding_dim, kernel_size=3, padding=1),
            nn.LogSoftmax(dim=1)
        )

    def forward(self, input):  # [B, L, C]
        x = input.permute(0, 2, 1)  # [B, C, L]
        x = self.conv_layers(x)

        x = x.permute(0, 2, 1)  # [B, L, C]

        x, _ = self.rnn1(x)
        x = self.drop1(x)

        x, _ = self.rnn2(x)
        x = self.drop2(x)

        x, _ = self.rnn3(x)
        x = self.drop3(x)

        x, _ = self.rnn4(x)
        x = self.drop4(x)

        x, _ = self.rnn5(x)
        x = self.drop5(x)

        x = x.permute(0, 2, 1)  # [B, C, L]
        x = self.post_rn_conv(x)

        x = x.permute(2, 0, 1)  # [L, B, C]
        return x
