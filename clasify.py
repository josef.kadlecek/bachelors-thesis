import torch
from model import Encoder_CNN_LSTM
from testing_and_validation import decode
from utils.config import Config
import numpy as np
from python_speech_features import mfcc
import soundfile as sf


def convert_to_mffc(file_path):
    data, sample_rate = sf.read(file_path)
    mfcc_features = mfcc(data, sample_rate)
    return mfcc_features


def load_sample(path_to_sample):
    if path_to_sample.endswith(".wav"):
        mfcc = convert_to_mffc(path_to_sample)
    else:
        mfcc = np.load(path_to_sample)

    mfcc = [torch.tensor(x).float() for x in mfcc]
    mfcc = torch.cat(mfcc).view(len(mfcc), 13)

    return mfcc


def clasify_sample(model, path_to_sample):
    mfcc = load_sample(path_to_sample)
    model.eval()
    model_output = model(mfcc.unsqueeze(-1).permute(2, 0, 1).to("cpu"))
    _, out = decode(model_output, None)
    print(out)


def init_model(config, model_path=None):
    model = Encoder_CNN_LSTM(len(config.alphabet))
    model.to("cpu")
    model.load_state_dict(torch.load(model_path))
    return model


if __name__ == "__main__":
    MODEL_PATH = "D:\\loss_0.6771159_cer_23.25552250624163"
    SAMPLE_PATH = "D:\\00018.wav"
    config = Config()
    model = init_model(config, MODEL_PATH)
    clasify_sample(model, SAMPLE_PATH)
