from torch.utils.data import Dataset, DataLoader
from utils.helping_fn import *
import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from pathlib import Path

class MyDataLoader:
    def __init__(self, config):
        batch_size = config.batch_size
        dataset_train = AudioDataset(config.training_manifest_path, batch_size, config)
        dataset_val = AudioDataset(config.val_manifest_path, batch_size, config)
        dataset_test = AudioDataset(config.test_manifest_path, 1, config)

        if torch.cuda.is_available():
            self.train_data = DataLoader(dataset_train, batch_size=batch_size, collate_fn=dataset_train.collate,
                                         pin_memory=True, num_workers=4, shuffle=True)
            self.val_data = DataLoader(dataset_val, batch_size=batch_size, collate_fn=dataset_val.collate,
                                       pin_memory=True, num_workers=4, shuffle=True)
            self.test_data = DataLoader(dataset_test, batch_size=1, collate_fn=dataset_test.collate,
                                        pin_memory=True, num_workers=4, shuffle=True)
        else:
            self.train_data = DataLoader(dataset_train, batch_size=batch_size, collate_fn=dataset_train.collate)
            self.val_data = DataLoader(dataset_val, batch_size=batch_size, collate_fn=dataset_val.collate)
            self.test_data = DataLoader(dataset_test, batch_size=1, collate_fn=dataset_test.collate)


class AudioDataset(Dataset):
    def __init__(self, root_path, batch_size, config):
        self.config = config
        self.files = files_from_manifest(root_path)
        self.batch_size = batch_size
        self.overfitting = config.overfitting

    def __len__(self):
        return len(self.files) - len(self.files) % self.config.batch_size

    def __getitem__(self, idx):
        mfcc = np.load(Path(self.config.dataset_root + "/" + self.files[idx] + "_mfcc.npy"))
        label = get_transcript(self.config.dataset_root + "/" + self.files[idx] + ".txt")
        label = labels_in_numpy(label)[0]

        return torch.tensor(mfcc).float(), len(mfcc), torch.tensor(label), len(label)

    def collate(self, batch):
        mfcc = pad_sequence([item[0] for item in batch])
        mfcc = mfcc.permute(1, 0, 2)
        mfcc_lens = torch.tensor([item[1] for item in batch])
        labels = pad_sequence([item[2] for item in batch])
        labels = labels.permute(1, 0)
        label_lens = torch.tensor([item[3] for item in batch])

        return mfcc.float(), mfcc_lens, labels, label_lens
