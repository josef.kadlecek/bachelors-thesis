import os
import shutil
import sys
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import torch
import math
import numpy as np

class Plotter():
    def __init__(self, config):
        self.config = config
        self.train_loss_history = []
        self.validation_loss_history = []
        self.validation_cer_history = []
        self.train_cer_history = []

    def update_history(self, train_loss, val_loss, train_cer, val_cer):
        self.train_loss_history.append(train_loss)
        self.validation_loss_history.append(val_loss)
        self.train_cer_history.append(train_cer)
        self.validation_cer_history.append(val_cer)
        np.save(os.path.join(self.config.experiment_folder, "train_loss_history"), self.train_loss_history)
        np.save(os.path.join(self.config.experiment_folder, "validation_loss_history"), self.validation_loss_history)
        np.save(os.path.join(self.config.experiment_folder, "train_cer_history"), self.train_cer_history)
        np.save(os.path.join(self.config.experiment_folder, "validation_cer_history"), self.validation_cer_history)

    def plot(self):
        def plot_one(title, ylabel, xlabel, data1, data2, name):
            self.updates_per_batch = 45760 / self.config.batch_size
            fig = plt.figure(figsize=(18.5, 10.5))
            axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
            plt.title(title)
            plt.ylabel(ylabel)
            plt.xlabel(xlabel)

            axes.plot([(i + 1) * self.updates_per_batch for i in range(len(data1))], data1, label="Training set")
            axes.plot([(i + 1) * self.updates_per_batch for i in range(len(data2))], data2, label="Validation set")
            plt.legend()

            plt.savefig(os.path.join(self.config.experiment_folder, name))
            plt.close('all')

        # Plot Loss
        plot_one('Loss function average per update', 'Avg loss', 'Update', self.train_loss_history,
                 self.validation_loss_history, "loss.jpg")

        # Plot CER
        plot_one('Character Error Rate history', 'CER %', 'Update', self.train_cer_history,
                 self.validation_cer_history, "cer.jpg")


class Logger():
    def __init__(self, config, model, dl, log=None):
        self.config = config
        self.dl = dl
        self.model = str(model)
        self.update = 0
        self.update_step = dl.train_data.__len__()
        self.experiment_folder = self.create_experiment_folder()
        self.log_file = os.path.join(self.config.experiment_folder, "training.log") if not log else log
        self.plotter = Plotter(config)
        self.best_val_cer = math.inf

    def flush(self):
        sys.stdout.flush()

    def create_experiment_folder(self):
        if os.path.isdir(self.config.experiment_folder):
            shutil.rmtree(self.config.experiment_folder)
        os.mkdir(self.config.experiment_folder)

        with open(os.path.join(self.config.experiment_folder, "model.txt"), 'w') as file:
            file.write(str(self.config) + "\n\n")
            file.write(self.model)
        return self.config.experiment_folder

    def log_decoding(self, result, cer, file=None):
        if file == None:
            file = os.path.join(self.config.experiment_folder, f"decoding_update_{self.update}.txt")

        with open(file, "w") as f:
            f.write(f"Testing CER: {cer}\n")
            for gt, res in result:
                f.write(f"{gt}\n{res}\n\n")

    def log_losses_and_cer(self, training_loss, training_cer, validation_loss, validation_cer):
        self.update += self.update_step
        self.plotter.update_history(training_loss, validation_loss, training_cer, validation_cer)

        with open(self.log_file, "a+") as f:
            f.write(f"Updates {self.update}\n")
            f.write(f"Training: loss:  {training_loss:.10f}     CER {training_cer:.5f}%\n")
            f.write(F"Validation loss: {validation_loss:.10f}     CER {validation_cer:.5f}%\n\n")
        self.plotter.plot()

    def save_model(self, model, val_cer):
        if val_cer < self.best_val_cer:
            print("New best model with CER:", val_cer)
            self.best_val_cer = val_cer
            torch.save(model.state_dict(), os.path.join(self.experiment_folder, "best_model"))
        else:
            print(f"Model {val_cer} not good enough previous best {self.best_val_cer}.")

    def alert(self, str):
        with open(self.log_file, "a+") as f:
            f.write(str)
