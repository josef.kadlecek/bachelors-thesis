import torch.nn as nn
import torch

class FacialFeaturesExtractor(nn.Module):
    def __init__(self, size=(32, 32), input_channels=1):
        super(FacialFeaturesExtractor, self).__init__()

        self.filters = 16
        self.vgg_blocks = 3

        last_channels = input_channels
        layers = []
        for stage in range(self.vgg_blocks):
            layers += [
                nn.Conv2d(last_channels, self.filters * 2**stage, kernel_size=3, stride=1, padding=1),
                nn.BatchNorm2d(self.filters * 2**stage),
                nn.LeakyReLU(negative_slope=0.05, inplace=True),
                nn.MaxPool2d(2)
            ]
            last_channels = self.filters * 2**stage
        self.layers = nn.Sequential(*layers)

        self.output = nn.Sequential(
            nn.Linear(last_channels * 16, 512),
            nn.BatchNorm1d(512),
            nn.LeakyReLU(),
        )

    def forward(self, x):
        x = self.layers(x)
        x = x.view(-1, 4*4*64)  # Make vector
        x = self.output(x)
        return x


class VideoModel(nn.Module):
    def __init__(self, embedding_dim):
        super(VideoModel, self).__init__()
        self.facial_features = FacialFeaturesExtractor()
        self.pooling_size = 1
        self.rnn_size = 1512
        self.dropout = 0.35

        self.conv_layers = nn.Sequential(
            nn.Conv1d(in_channels=512, out_channels=self.rnn_size, kernel_size=3, padding=1),  # Mini-batch * 128 * len
            nn.BatchNorm1d(self.rnn_size),
            nn.LeakyReLU(negative_slope=0.05),
        )

        self.gru1 = nn.LSTM(input_size=self.rnn_size, hidden_size=self.rnn_size // 2, batch_first=True,
                           bidirectional=True)
        self.gru2 = nn.LSTM(input_size=self.rnn_size, hidden_size=self.rnn_size // 2, batch_first=True,
                           bidirectional=True)
        self.gru3 = nn.LSTM(input_size=self.rnn_size, hidden_size=self.rnn_size // 2, batch_first=True,
                           bidirectional=True)
        self.gru4 = nn.LSTM(input_size=self.rnn_size, hidden_size=self.rnn_size // 2, batch_first=True,
                           bidirectional=True)
        self.gru5 = nn.LSTM(input_size=self.rnn_size, hidden_size=self.rnn_size // 2, batch_first=True,
                            bidirectional=True)

        self.drop0 = nn.Dropout(0.1)
        self.drop1 = nn.Dropout(self.dropout)
        self.drop2 = nn.Dropout(self.dropout)
        self.drop3 = nn.Dropout(self.dropout)
        self.drop4 = nn.Dropout(self.dropout)
        self.drop5 = nn.Dropout(self.dropout)

        self.pre_fc_conv = nn.Sequential(
            nn.BatchNorm1d(self.rnn_size),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Conv1d(in_channels=self.rnn_size, out_channels=1024, kernel_size=3, padding=1),
            nn.BatchNorm1d(1024),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Conv1d(in_channels=1024, out_channels=512, kernel_size=3, padding=1),
            nn.BatchNorm1d(512),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Conv1d(in_channels=512, out_channels=embedding_dim, kernel_size=3, padding=1),
            nn.LogSoftmax(dim=1)
        )


    def forward(self, x):  # [FRAMELEN, B, H, W]
        input_shape = list(x.shape)
        x = x.view(-1, *x.shape[2:])
        x = self.facial_features(x)
        x = x.view(input_shape[0], input_shape[1], x.shape[1])

        x = x.permute(1, 2, 0)  # [B, C, L]
        x = self.conv_layers(x)

        x = x.permute(0, 2, 1)  # [B, L, C]

        x, _ = self.gru1(x)
        x = self.drop1(x)

        x, _ = self.gru2(x)
        x = self.drop2(x)

        x, _ = self.gru3(x)
        x = self.drop3(x)

        x, _ = self.gru4(x)
        x = self.drop4(x)

        x, _ = self.gru5(x)
        x = self.drop5(x)


        x = x.permute(0, 2, 1)  # [B, C, L]
        x = self.pre_fc_conv(x)

        x = x.permute(2, 0, 1)
        return x  # Desired [L, B, C]
